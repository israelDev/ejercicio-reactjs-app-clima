import React, { Fragment, useState, useEffect } from 'react';
import Header from './component/Header'
import Formuilario from './component/Formulario'
import Clima from './component/Clima'
import Error from './component/Error'

function App() {

  const [busqueda, guardarBusqueda] = useState({
    ciudad: '',
    pais: ''
  })

  const { ciudad, pais } = busqueda;
  const [consultar, guardarConsultar] = useState(false)
  const [resultado, guardarResultado] = useState({})
  const [error, guardarError] = useState(false)
  let componente;


  useEffect(() => {

    const consultarApi = async () => {
      if (consultar) {
        const key = 'da7f2f0e675576002f2e46d1782ba86b'
        const url = `https://api.openweathermap.org/data/2.5/weather?q=${ciudad},${pais}&appid=${key}`
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        guardarResultado(resultado)
        guardarConsultar(false)

        console.log(resultado)

          // Detecta si hubo resultados correctos en la consulta

          if(resultado.cod === "404") {
              guardarError(true);
          } else {
              guardarError(false);
          }
      }
    }

    consultarApi()

  }, [consultar])

  if(error) {
    componente = <Error mensaje="No hay resultados" />
  } else {
    componente = <Clima 
                    resultado={resultado}
                />
  }


  return (
    <Fragment>
      <Header
        titulo='Clima React App'
      />
      <div className="contenedor-form">
        <div className="container">
          <div className="row">
            <div className="col m6 s12">
              <Formuilario
                busqueda={busqueda}
                guardarBusqueda={guardarBusqueda}
                guardarConsultar={guardarConsultar}
              />
            </div>
            <div className="col m6 s12">
              {componente}
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
}

export default App;
